 <!DOCTYPE html>
<html lang="en-US">
<script src="angular_js.js"></script>
<body>

<div ng-app="myApp" ng-controller="myCtrl">

				<p>Name : <input type="text" ng-model="name"></p>
				<h1>Hello ->  {{name}}</h1>

				First Name: <input type="text" ng-model="firstName"><br><--------------------------------------------------------------------------><br>
				Last Name: <input type="text" ng-model="lastName"><br><--------------------------------------------------------------------------><br>
				<br><--------------------------------------------------------------------------><br>
				Full Name: {{firstName + " " + lastName}}
				<br><--------------------------------------------------------------------------><br>
				Full Name with function : {{fullName }}
				<br><--------------------------------------------------------------------------><br>
				<welcome></welcome>
				<br><--------------------------------------------------------------------------><br>
				 <div ng-app="" ng-init="quantity=1;price=5">

				Quantity: <input type="number"	ng-model="quantity">
				Costs:    <input type="number" ng-model="price">

				Total in dollar: {{ quantity * price }}

				</div>
				<br><--------------------------------------------------------------------------><br>
				<input type="text" ng-model="test.myString" />
				<h2>
				 Capital the letter {{ test.myString | capitalize }}
				</h2>
				<br><--------------------------------------------------------------------------><br>

<p ng-show="true">I am visible.</p>

<p ng-show="false">I am not visible.</p>
				<br><--------------------------------------------------------------------------><br>

				<div ng-init="names=['jerome','jose','bay','max']">
				iteration with data
				<li ng-repeat="name in names">
				{{ name }}
				</li>

				</div>
				<br><--------------------------------------------------------------------------><br>

				<div ng-init="fnames=[{ fname:'aaa'},{fname:'bbbb'}]">
				for each data with uppecase
				<li ng-repeat="name in fnames | orderBy:'fname' ">
				{{ name.fname | uppercase }}
				</li>

				</div>
				<br><--------------------------------------------------------------------------><br><-------------------------------------------------------------------------->


				<div ng-init="fnames=[{ fname:'aaa'},{fname:'bbaab'},{fname:'cccaa'},{fname:'ddaad'},{fname:'eeaa'},{fname:'ffaa'},{fname:'ggg'},{fname:'hh'}]">
				Search Data : 
				<input type="text" ng-model="nameText" />
				<li ng-repeat="name in fnames  | filter:nameText  ">
				{{ name.fname | uppercase }}
				</li>

				</div>

				<br><--------------------------------------------------------------------------><br>

				<div ng-init="fnames=[{ fname:'111aaa'},{ fname:'222aaa'},{ fname:'33aaa'},{fname:'bbaab'},{fname:'cccaa'},{fname:'ddaad'},{fname:'eeaa'},{fname:'ffaa'},{fname:'ggg'},{fname:'hh'}]">
				 Order by Search Data : 
				<input type="text" ng-model="nameText" />
				<li ng-repeat="name in fnames  | filter:nameText | orderBy:fname:reverse ">
				{{ name.fname | uppercase }}
				</li>

				</div>

				<br><--------------------------------------------------------------------------><br>

				<div  ng-controller="myCtrl as samplecontroller">

		<input type="text" ng-model="nameText" placeholder="Search Name" />
				<div >
				 Name with Controller Function using alias : 
				
				<li ng-repeat="name in samplecontroller.fnames  | filter:nameText | orderBy:'fname' ">
				{{ name.firstname  }}
				</li>

				</div>
</div>
				<br><--------------------------------------------------------------------------><br>
				<br><--------------------------------------------------------------------------><br>
				<br><--------------------------------------------------------------------------><br>
				<br><--------------------------------------------------------------------------><br>
				<br><--------------------------------------------------------------------------><br>
				<br><--------------------------------------------------------------------------><br>
				<br><--------------------------------------------------------------------------><br>
				<br><--------------------------------------------------------------------------><br>
				<br><--------------------------------------------------------------------------><br>
				<br><--------------------------------------------------------------------------><br>
				<br><--------------------------------------------------------------------------><br>
				<br><--------------------------------------------------------------------------><br>
				<br><--------------------------------------------------------------------------><br>
				<br><--------------------------------------------------------------------------><br>
				<br><--------------------------------------------------------------------------><br>
				<br><--------------------------------------------------------------------------><br>




</div>

<script>
var app = angular.module('myApp', []);

app.directive("welcome", function() {
  return {
    restrict: "E",
    template: "<div>Howdy there! You look splendid.</div>"
  }
});

app.controller('myCtrl', TestCtrl);
// define a filter called 'capitalize' that will invoke the CapitalizeFilter function
app.filter('capitalize', CapitalizeFilter);

function TestCtrl($scope) {
  // basic controller where we preset the scope myString variable
  var self = this;
  self.myString = "hello world";
	$scope.fullName =  "firstname function ";

}

// this is where the filter magic happens.
function CapitalizeFilter() {
  // this is the function that Angular will execute when the expression is evaluated
  return function (text) {
    // text is the original string output of the Angular expression
    return text.toUpperCase();
    // and we simply return the text in upper case!
  }
}


app.controller('myCtrl',testcontroller);
function testcontroller(){
	this.fnames = [
				{ 	firstname:'ram'},
				{ 	firstname:'ron'},
				{ 	firstname:'aziel'},
				{	firstname:'mic'},
				{	firstname:'mark'},
				{	firstname:'jeff'},
				{	firstname:'ian'},
				{	firstname:'dom'},
				{	firstname:'jose'},
				{	firstname:'baymax'}
				];

}

</script> 


</body>
</html> 