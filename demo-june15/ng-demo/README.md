# ng-demo
AngularJS Fundamentals Demo

 - install nodejs, git, mongodb
 - download this repo
 - using git cd $path_of_your_repo
 - install global npm (npm install -g grunt grunt-cli express bower)
 - execute the following command at your git bash (bower install and npm install)
 - using git bash, sh mongodb.sh (this will run your mongodb instance)
 - execute grunt serve
