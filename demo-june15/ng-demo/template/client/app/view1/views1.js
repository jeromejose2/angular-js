'use strict';

angular
  .module('ngDemo1App')
  .config(function ($stateProvider) {
	    $stateProvider
	      .state('view1', {
	        url: '/view1',
	        templateUrl: 'app/view1/main.html',
	        controller: 'View1Ctrl'
	      });
  });