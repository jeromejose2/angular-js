'use strict';

angular
  .module('ngDemo1App')
  .controller('View1Ctrl', View1Ctrl);

View1Ctrl.$inject = ['$scope', 'Contacts', 'notifications', '$timeout'];

function View1Ctrl($scope, Contacts, notifications, $timeout) {	
View1.get()
	  	.success(function(data, status, headers, config){
			$scope.contacts = data;
	  	})
		.error(function(data, status, headers, config){
	  	   	console.log(data);
	  	});

};