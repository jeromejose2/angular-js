(function() {
    'use strict';
    var controller = (function() {

        function controller() {
            this.car = "CAR";
            this.building = "BUILDING";
            this.place = "PLACE";
            this.name = " NAME";

        }

        controller.prototype.changeName = function() {
            this.name = "call the ChangeNAME()"
        }

        return controller;
    })();

    var helloWorld = function() {
        return {

		            scope: {
		                place: '@'
		            },
		            //replace: true, remove the existing tag
		            template: '<h1>Hello World test -using @ directive {{place}} </h1>'
		        }
    };

    var helloJerome = function() {
        return {
            //replace: true, remove the existing tag
            template: '<h1>Jerome tag - Car {{ vm.car }}</h1>MALLS {{ building }}'
        }
    };

    var helloJeromeJose = function() {
        return {

		            scope: {
		                name: '='
		            },
		            //replace: true, remove the existing tag
		            templateUrl: 'twowaybinding.html'
		                //template : '<h1>Isolate Scope</h1></br><input type="text" ng-model="name" /><br><button ng-click="name=\'James\'"> Change name </button>'
		        }
    };
    var helloChangeName = function() {
        return {

		            scope: {
		                changeName: '&'
		            },
		            template: "<button ng-click='changeName()'> Change name </button>"
		        }
    };




    angular
        .module('demo', [])
        .controller('ShareScopeController', controller)
        .directive('helloWorldJerome', helloWorld)
        .directive('jerome', helloJerome)
        .directive('jeromejose', helloJeromeJose)
        .directive('changename', helloChangeName);


})();
