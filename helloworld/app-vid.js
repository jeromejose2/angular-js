(function() {
    'use strict';


var helloWorldController = (function(){
function helloWorldController()
{
	this.hello ="Hello World";
}
helloWorldController.prototype.clickMe = function(){
	this.hello = "Hello Planet";
	this.name =  "Hello Planet";
};

return helloWorldController;
})();

var helloWorld = function(){
	return {
		scope:{
			name: '@'
		},
		bindToController: true,
		template :'<h1>From Parent {{vm.name}}</h1>{{ vm.hello }}<button ng-click="vm.clickMe()">Clickme</button>',
		controller: helloWorldController,
		controllerAs: 'vm'
	}
}
   var parentController = (function(){
   	function parentController(){
   		this.name = 'Pedro';

   	}
   	return parentController;
   })();
    angular
        .module('demo', [])
        .controller('ParentController',parentController)
        .directive('helloWorld', helloWorld);


})();
